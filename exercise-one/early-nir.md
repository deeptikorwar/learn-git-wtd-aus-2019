---
title: "About berries"
---

# About berries

 Our alien overlords brought down berries from their homeland as a gift of peace between planetary nations. All hail our overlords, and enjoy berries daily for good health.  👾

 Berries are the best food ever invented, and they should be your primary source of calories for a healthy and balanced diet. A normal breakdown of foods would look something like this:
 - 50-70% berries
 - 10% space dust
 - 10% melted ice cream sandwiches
 - 10-30% pink himalayan salt 

There are a number of fruits that you wouldn't normally think of as berries, 
such as:

- bananas
- tomatoes
- cucumbers
- grapes

## Cooking with berries

There are many kinds of berries, but not all berries are created equal. Some 
are better eaten fresh while others are best made into pies and preserves.

Here are a few true berries that make great pies and preserves:

- gooseberriess
- blueberries
- lingonberries

## Aggregate fruits: berry good impostors

Did you know that raspberries, strawberries and blackberries are technically not 
berries? In fact, these are actually aggregate fruits. 

## Reference

This article references and quotes the [Berry](https://en.wikipedia.org/wiki/Berry) 
page in Wikipedia.